#!/bin/sh
#
# The MIT License (MIT)
#
# Copyright (c) 2016 Fumiyuki Shimizu, Abacus Technologies, Inc.
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

umask 077
PATH='/bin:/usr/bin:/usr/sbin'

OPENSSL='/usr/local/bin/openssl'

# For Postfix
#   smtpd_tls_dh1024_param_file = /var/db/ssl/dh2048.pem
#   smtpd_tls_dh512_param_file =  /var/db/ssl/dh512.pem
# For Apache HTTPD
#   SSLCertificateFile	/var/db/ssl/...._param.pem

DESTDIR='/var/db/ssl'

DHBITS='512 1024 2048'

# /usr/local/bin/openssl ecparam -list_curves
#
# strong and inter operability:
#   sect571r1 secp521r1 secp384r1 prime256v1
# speed:
#   secp384r1 prime256v1
ECCURVES='secp384r1 prime256v1'
DHPARAM='2048'

CERTDIR='/etc/ssl/certs'
CERT0='a/hoge.bundle.pem'
CERT1='a/hoge.crt'
CERT2='b/hero.pem'
#CERT3=
#CERT4=
#CERT5=



BN=$(basename "$0")
TMPD=$(mktemp -d "/tmp/.$BN.XXXXXXXXXXX")
if [ -n "$TMPD" ] && cd "$TMPD"; then
  # created
else
  echo "Cannot create a temporary directory: $TMPD"
  exit 1
fi
TMP1=$(mktemp     "$TMPD/.$BN.1.XXXXXXXXXX")
TMPD2=$(mktemp -d "$TMPD/.$BN.2.XXXXXXXXXX")

rc=1
cleanup () {
  rm -rf "$TMPD"
  exit $rc
}
trap 'cleanup' EXIT TERM



mkdir -p "$DESTDIR"
chmod a=x "$DESTDIR"
cd "$DESTDIR" || { echo "Cannot create a directory: $DESTDIR"; exit; }

for b in $DHBITS; do
  cat /dev/null >"$TMP1"
  touch -A-030000 "$TMP1"
  PFILE="$DESTDIR/dh$b.pem"
  if [ ! -s "$PFILE" -o \( "$PFILE" -ot "$TMP1" \) ]; then
    test -t 0 && echo "DH param: ${b}bits"
    if "$OPENSSL" dhparam -out "$TMP1" "$b" 2>/dev/null; then
      if [ -s "$TMP1" -a -r "$PFILE" ] && diff -q "$PFILE" "$TMP1" >/dev/null; then
        # same
      else
        cat "$TMP1" >"$PFILE"
      fi
    fi
  fi
  chmod a=r "$PFILE"
  test -t 0 && echo "DH param file: $PFILE"
done

for c in $ECCURVES; do
  cat /dev/null >"$TMP1"
  test -t 0 && echo "EC param: $c"
  "$OPENSSL" ecparam -out "$TMPD2/$c" -name "$c" || rm -fv "$TMPD2/$c"
done

n=0
while true; do
  eval pem=\"\$CERT$n\"
  [ -z "$pem" ] && break
  cat "$CERTDIR/$pem"            >"$TMP1"
  for c in $ECCURVES; do
    cat "$TMPD2/$c"             >>"$TMP1"
  done
  cat "$DESTDIR/dh$DHPARAM.pem" >>"$TMP1"

  D="$DESTDIR"/$(dirname "$pem")
  mkdir -p "$D"
  chmod a=x "$D"
  cd "$D" || { echo "Cannot create a directory: $D"; exit; }

  CRTFILE="$D"/$(basename $(basename "$pem" .pem) .crt)'_param.pem'
  if [ -s "$CRTFILE" -a -r "$CRTFILE" ] && diff -q "$CRTFILE" "$TMP1" >/dev/null; then
    # same
  else
    cat "$TMP1" >"$CRTFILE"
  fi
  chmod a=r "$CRTFILE"
  test -t 0 && echo "Cert. with EC and DH parameters: $CRTFILE"

  n=$(expr "$n" + 1)
done

rc=0

# end of file
